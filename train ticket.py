"""
作者：纪元
文件名称：实时车次信息
时间：2020年3月25日

"""

g = '{:^5}\t\t{:^7}\t\t{:^5}\t\t{:^5}\t\t{:^5}'
T40 = g.format("T40", "长春-北京", "00:12", "12:20", "12:08")
T298 = g.format("T298", "长春-北京", "00:06", "10:15", "10:44")
Z158 = g.format("Z158", "长春-北京", "12:48", "21:06", "08:18")
Z62 = g.format("Z62", "长春-北京", "21:58", "06:08", "8:20")
timetable = [T40, T298, Z158, Z62]

x = int(input("1.更新车次信息   2.打印车次信息"))
if x == 1:
    print("请输入要更新的车次信息")
    a = input("车次：")
    b = input("出发站-到达站：")
    c = input("出发时间：")
    d = input("到达时间：")
    e = input("历时：")
    a = g.format(a, b, c, d, e)
    timetable.append(a)
    print("实时车次信息：")
    print("车次", "      出发站-到达站", "   出发时间", "   到达时间", "     历时")
    for item in timetable:
        print(item, end="\n")
elif x == 2:
    print("实时车次信息：")
    print("车次", "      出发站-到达站", "   出发时间", "   到达时间", "     历时")
    for item in timetable:
        print(item, end="\n")