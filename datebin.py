"""
作者：纪元
文件名称：数据库
时间：2020年4月29日
"""
import sqlite3
conn = sqlite3.connect('wolf.db')
cursor = conn.cursor()
cursor.execute('create table if not exists wolf (id, name)')
cursor.execute('insert into wolf (id, name) values (01, "甲"), (02, "丙"), (10, "癸"), (99, "九")')
cursor.execute('delete from wolf where id = 99')
cursor.execute('update wolf set name ="乙" where id = 02')
cursor.execute('select * from wolf')
print("wolf数据库：\n", cursor.fetchall())
cursor.close()
conn.close()