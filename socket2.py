"""
作者：纪元
文件名称：socket
时间：2020年5月23日
"""
import socket
import base64
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('127.0.0.1', 8001))
s.listen()
conn, address = s.accept()
name = conn.recv(1024)
print("来自", address, "的文件：", name.decode())
data = conn.recv(1024)
f = open("receive.txt", "w")
data1 = base64.b64decode(data)
f.write(data1.decode("utf-8"))
f.close()
data1 = conn.recv(1024)
f = open("receive.txt", "r")
data1 = f.read()
conn.sendall(data1.encode())
f.close()
data1 = conn.recv(1024)
print("来自", address, "的信息", data1.decode())
s.close()