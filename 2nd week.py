"""
作者：纪元
文件名称：Python语言基础实践
时间：2020年3月4日

"""

x = int(input("在0~26间输入加密整数"))
a = int(5 * x + 8) % 26
print("密码为",a)
b = 21 * (a - 8) % 26
print("解密后为",b)
if x == b :
    print("解密成功")
if x != b :
    print("解密失败")

print("\n")
a = int(input("输入首项"))
n = int(input("输入项数"))
q = int(input("输入公比"))
S = a * ((q ** n) - 1) / (q - 1)
print("求和结果",S)